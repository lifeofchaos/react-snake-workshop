Snake game made with React for "Webinars for Dummies I" by Santiago Cerro López.

## Description

This proyect contains homemade version of Snake game. The objetive of this proyect is teach React to participants of Webinars for Dummies I Edition.

This is not a product suitable for production environment because it have non optimized code to keep simplicity for the course.

## How to start

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. You can change the code and view changes in real time.

Build project

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />

Hope you enjoy it!