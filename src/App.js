import React from 'react';
// We import our Food component
import SnakeFood from './components/SnakeFood';
// We import our Snake Component
import Snake from './components/Snake';
import './App.css';

// We need to create some methods for our app, for this reason we need to extend React
class App extends React.Component {
  constructor(props) {
    super(props);
    // React State comes to help us!
    this.state = {
      // Define the default food coordinates
      foodCoordinates: [0, 0],
      // We define a Snake with 3 body blocks in horizontal
      snake: [[0, 0], [2, 0], [4, 0]],
      // We define snake direction, RIGHT by default
      direction: 'RIGHT',
      // Default speed
      speed: 200
    };
    // We need to bind this in our grow snake onClick function
    this._growSnake = this._growSnake.bind(this);
    // We need to bind this in our move snake onClick function
    this._moveSnake = this._moveSnake.bind(this);
    // We need to bind this in our keyDown listener function
    this._keyPressEventListener = this._keyPressEventListener.bind(this);
  }
  // We need to register keyDown Event on component mount
  componentDidMount() {
    document.addEventListener('keydown', this._keyPressEventListener);
    // When component is mounted it means the game is starting
    // for that reason we generate new food location
    this._generateNewFoodCoordinates();
    // We add interval for auto move
    window.snakeInterval = setInterval(this._moveSnake, this.state.speed);
  }
  // As a good practice, we need to remove keyDown Event on component unmount
  componentWillUnmount() {
    document.removeEventListener('keydown', this._keyPressEventListener);
    clearInterval(window.snakeInterval);
  }
  // We need to check some snake props when DOM is updated
  componentDidUpdate(prevProps, prevState, snapshot) {
    this._isEating();
    this._isCollide();
    this._checkIfOutsideGameArea();
  }
  // Check if snake is "eating" the food
  _isEating() {
    let snake = this.state.snake;
    let food = this.state.foodCoordinates;
    let snakeHead = snake[snake.length - 1];
    // If food is in snake head, snake will grow
    if (snakeHead[0] === food[0] && snakeHead[1] === food[1]) {
      this._growSnake();
      this._generateNewFoodCoordinates();
    }
  }
  // Check if collision
  _isCollide() {
    // Store locally
    let snake = [...this.state.snake];
    let snakeHead = snake[snake.length - 1];
    // Remove the head
    snake.pop();
    // Collide is false by default
    let collide = false;
    snake.forEach(coordinates => {
      // if the head is in the same position than a body part, it means it collide
      if (coordinates[0] === snakeHead[0] && coordinates[1] === snakeHead[1]) {
        collide = true;
      }
    });
    // If collide, game over
    if (collide) {
      this._restartGame();
    }
  }
  // Check if is outside borders
  _checkIfOutsideGameArea() {
    let snake = [...this.state.snake];
    let snakeHead = snake[snake.length - 1];
    if (snakeHead[0] >= 100 || snakeHead[1] >= 100 || snakeHead[0] < 0 || snakeHead[1] < 0) {
      this._restartGame();
    }
  }
  // Coordinates generator for food
  _generateNewFoodCoordinates() {
    // We only can generate points between 0 and 98 because 100 is a border
    let [min, max] = [0, 98];
    // Generate X and Y coordinates
    let [x, y] = [
      // We are generating a random number
      // if we divide by 2 and later multiply by 2
      // we are getting only numbers that can be divided by 2
      Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2,
      Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2
    ];
    // Point is not valid by default
    let isValidPoint = false;
    // Store our food coordinates
    let foodCoordinates = this.state.foodCoordinates;
    // With this loop we will ensure that food is not in body snake and food is in a different location
    while (!isValidPoint) {
      // Easy way: we create a collide flag
      let collide = false;
      this.state.snake.forEach(coordinate => {
        // Check if food collide with snake
        if (coordinate[0] === x && coordinate[1] === y) {
          collide = true;
        }
      });
      // Check if food is in the same location
      if (foodCoordinates[0] === x && foodCoordinates[1] === y) {
        collide = true;
      }
      // If there's no problem, we can move the food
      if(!collide) {
        isValidPoint = true;
      }
    }
    // Save the state
    this.setState({
      foodCoordinates: [x, y]
    });
  }
  // Let's use the keys instead of ugly buttons
  _keyPressEventListener(e) {
    let key = e.keyCode;
    switch (key) {
      case 38: //Key Up
      case 87: //Key W
        this._moveSnake('UP');
        break;
      case 37: // Key Left
      case 65: // Key A
        this._moveSnake('LEFT');
        break;
      case 40: // Key Down
      case 83: // Key S
        this._moveSnake('DOWN');
        break;
      case 39: // Key Right
      case 68: // Key D
        this._moveSnake('RIGHT');
        break;
      default:
        break;
    }
  }
  _growSnake() {
    // Store snake locally
    let newSnake = [...this.state.snake];
    // Get the head (last element of array)
    let snakeHead = newSnake[newSnake.length - 1];
    // Add one body part in front of snake head inf front of the head
    // We need to check direction before grow and grow in that direction
    switch (this.state.direction) {
      case 'RIGHT':
        newSnake.push([snakeHead[0] + 2, snakeHead[1]]);
        break;
      case 'LEFT':
        newSnake.push([snakeHead[0] - 2, snakeHead[1]]);
        break;
      case 'UP':
        newSnake.push([snakeHead[0], snakeHead[1] - 2]);
        break;
      case 'DOWN':
        newSnake.push([snakeHead[0], snakeHead[1] + 2]);
        break;
      default:
        break;
    }
    // Save the state
    this.setState({ snake: newSnake });
    // More speed
    this._changeSpeed();
  }
  // Speed util
  _changeSpeed() {
    let speed = this.state.speed;
    // We cannot set interval to 0
    if (speed > 10) {
      // reduce 10 units
      speed -= 10;
      // Save state and call regenerateInterval()
      this.setState({ speed }, this._regenerateInterval);
    }
  }
  // Refresh interval
  _regenerateInterval(force = false) {
    let speed = this.state.speed;
    if (force) {
      speed = 200;
    }
    clearInterval(window.snakeInterval);
    window.snakeInterval = setInterval(this._moveSnake, speed);
  }
  // Let's add some movement
  _moveSnake(direction) {
    // Little change: if no direction is passed we use state stored direction
    if (!direction) {
      direction = this.state.direction;
    }
    // We need to check last move to avoid strange movements
    // For example if snake is moving left, snake can't move right (return)
    let currentDirection = this.state.direction;
    if (
      (currentDirection === 'RIGHT' && direction === 'LEFT') ||
      (currentDirection === 'LEFT' && direction === 'RIGHT') ||
      (currentDirection === 'UP' && direction === 'DOWN') ||
      (currentDirection === 'DOWN' && direction === 'UP')
    ) {
      return;
    }
    // As always store snake locally
    let newSnake = [...this.state.snake];
    // Get the head
    let snakeHead = newSnake[newSnake.length - 1];
    /*
    * Some Maths:
    * Left: only moves X, negative
    * Right: only moves Y, positive
    * Up: only moves Y, negative
    * Down: only moves Y, positive
    * Why? Because we aren't in 0,0, we start in (-100, 100)
    */
    switch (direction) {
      case 'LEFT':
        snakeHead = [snakeHead[0] - 2, snakeHead[1]];
        break;
      case 'RIGHT':
        snakeHead = [snakeHead[0] + 2, snakeHead[1]];
        break;
      case 'UP':
        snakeHead = [snakeHead[0], snakeHead[1] - 2];
        break;
      case 'DOWN':
        snakeHead = [snakeHead[0], snakeHead[1] + 2];
        break;
      default:
        break;
    }
    // Let's make it easy
    // 1. Add one body part
    newSnake.push(snakeHead);
    // 2. Remove the tail
    newSnake.shift();
    // 3. Save the state
    // Easy peasy :)
    this.setState({
      snake: newSnake,
      direction
    });
    /*
    * Other way to to this is with Array.map, sum 2 units to body part.
    * Is slow and is more complex, but you are free to use it if you love.
    * Remember: [+ 2] depends on the movement
    *  newSnake = newSnake.map(part => {
    *    return [part[0] [+ 2], part[1] [+ 2]];
    *  });
    *  this.setState({ snake: newSnake });
    * Or with less code
    *  this.setState({
    *    snake: newSnake.map(part => {
    *      return [part[0] [+ 2], part[1] [+ 2]];
    *    })
    *  });
    */
  }
  // Restart game function. It will show an alert and snake length
  _restartGame() {
    alert(`Game over. Snake length is ${this.state.snake.length}. Game is restarting`);
    // Restore default state
    this.setState({
      snake: [[0, 0], [2, 0], [4, 0]],
      foodCoordinates: [0, 0],
      speed: 200,
      direction: 'RIGHT'
    });
    // Generate new food position
    this._generateNewFoodCoordinates();
    // Regenerate interval
    this._regenerateInterval(true);
  }
  render() {
    // We add our food like an HTML tag: <SnakeFood/>
    // SnakeFood name is defined by the import
    // Snake name is defined by the import
    return (
      <div className="snake_board">
        <Snake body={this.state.snake}/>
        <SnakeFood coordinates={this.state.foodCoordinates}/>
      </div>
    );
  }
}

export default App;
