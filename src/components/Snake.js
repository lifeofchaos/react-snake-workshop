// Is a React Component, for this reason we need to import React
import React from 'react';

// All React components extends React.Component
class Snake extends React.Component {
  // We create constructor and set State
  constructor(props) {
    super(props);
    // We need position be an array, for that reason we add a default empty array
    // Example: [[0, 0], [0, 2]]
    // Every element is composed by X and Y coordinates
    this.state = {
      body: props.body | []
    };
  }
  _position(coordinates) {
    let [x = 0, y = 0] = coordinates;
    return {
      left: `${x}%`,
      top: `${y}%`,
    }
  }
  // As we learned in past commits we need to update state if necessary and force render too
  static getDerivedStateFromProps (props, state) {
    // We are always updating because props will be changed every second
    return {
      body: props.body
    }
  }
  // Remember: render() is mandatory
  // React.Fragments allows us to repeat a component without wrap it inside other
  // without React.Fragment our output needs to be something like
  // <div><div class="snake_body_part"/><div class="snake_body_part"/></div>
  // and we dont want it.
  render() {
    // Loops in React. This construction is the most used.
    // Other way could be the use of a empty var, Array.forEach and append JSX string to this var
    // and finally return var content
    // Remember: React ne
    return (
      <React.Fragment>
        {this.state.body.map((coordinates, index) => {
          return <div className="snake_body_part" style={this._position(coordinates)} key={index}/>;
        })}
      </React.Fragment>
    );
  }
}

// Don't forget export it for use in other files
export default Snake;
