// We always need to import React
import React from 'react';

// Our component needs to extend React Component
class SnakeFood extends React.Component {
  // We need to create a constructor function. Constructor always receive props.
  // Props needs to be passed as argument to parent constructor.
  constructor(props) {
    super(props);
    // We add a little change, we will pass coordinates as array
    // We learn a lot about states, let´s use it
    this.state = {
      coordinates: props.coordinates
    };
  }
  // This method is called before render() and it help us to update coordinates state from props
  static getDerivedStateFromProps (props, state) {
    // Our main condition is that X and Y needs to be divisible by 2
    // and the other is that coordinates needs to be different
    if ((props.coordinates[0] % 2 !== 0 || props.coordinates[1] % 2 !== 0) && props.coordinates !== state.coordinates) {
      // return null means that render function will not be called
      return null;
    }
    // We return the new state object and render function will be called
    return {
      coordinates: props.coordinates
    }
  }
  
  // We create a private function to return left and top properties from coordinates
  _position() {
    // Destructuring coordinates into x, y with default value of 0
    let [x = 0, y = 0] = this.state.coordinates;
    return {
      left: `${x}%`,
      top: `${y}%`,
    }
  }
  // Our component needs a render function
  render() {
    // render always need return something other way component will fail!
    return (
      <div className="snake_food" style={this._position()}/>
    );
  }
}

export default SnakeFood;
